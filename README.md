Sentinel
====

Projeto Symfony iniciado em 11/03/2016 para atendimento ao teste backend Possible.


Detalhes
===
O projeto tem por finalidade primária em ser um scanner que verifica links/imagens quebrados e textos "lorem ipsum" esquecidos. Basta informar endereço e aguardar pelos resultados.

Aproveitando a deixa "Matrixiana", utilizei a mesma referência para a dependência de objetos: uma máquina Octupus depende de um Crawler (rastreador) para scanear por human.... estruturas HTML!

Posso seguir qualquer especificação de programação, como não tenho algum requisito disso o projeto foi construído em inglês e os comentários no código estão em português.


Dependências e Configurações
===
- [SensioBuzzBundle](https://github.com/sensiolabs/SensioBuzzBundle)
- [The DomCrawler Component]( http://symfony.com/doc/current/components/dom_crawler.html)
- Verifique o arquivo composer.json para mais detalhes.
- Existe um arquivo .json dentro do bundle em Extra, que possui os trechos 1.10.32 e 1.10.33 de "Finibus Bonorum et Malorum" e de uma passagem padrão de Lorem ipsum, das quais são gealmente utilizadas por geradores de lorem ipsum. Objetivo: encontrar os termos em textos na página. Para adicionar mais trechos em latim, basta alterar o mesmo arquivo.


Como Usar (interface web)
===
1. Informa uma URL no campo de pesquisa.
2. Enviar a sentinela.
3. Tomar um café.
4. Verificar os resultados (resultados em vermelho denotam links quebrados).

Exemplo de relatório com resultados pode ser verificado em web/examples/page_result.pdf.

Como usar (linha de comando)
===
1. Em /sentinel informe php bin/console sentinel -h para ajuda
2. Acompanhe as mensagens e a barra de progresso (pode acompanhar com um café).
3. Verifique os resultados (resultados em vermelho denotam links quebrados).

Exemplos:

![console](web/examples/console.png "Console")

![console2](web/examples/console_lorem.png "Console Lorem")


Roadmap
===
10/03/06: iniciando os estudos em Symfony com uma série de screencasts e muita documentação. (3,5 horas)

11/03/06: desenvolvido uma série de experimentos com componentes Symfony, nasce Sentinel! (4 horas)

12/03/06: nada implementado, ~~pausa para kung fu e aula com o Morpheus~~.

13/03/06: scanner finalizado (backend), melhorando funcionalidades, aplicando mais testes. (7,5 horas)

14/03/06: melhorias no retorno de dados e dos testes phpunit. (4 horas)

15/03/06: criação da interface por linha de comando (muito divertido). (4 horas)


ToDo
==

- Ainda tentando resolver o problema de Recv failure: Connection reset by peer do componente vendor/kriswallsmith/buzz/lib/Buzz/Client/Curl.php para sites com mais de 200 links. O host onde esta aplicação roda pode evitar esta problemática.
- Desenvolver visual melhor para a aplicação ~~contrata-se frontend, paga-se em cerveja~~.
- Melhorar os testes em todo o projeto, principalmente pela interface em linha de comando.
- Implementar uma forma de exportar os resultados (txt talvez).


