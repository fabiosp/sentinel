<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Machine\Octopus;
use AppBundle\Validation\ValidatePage;
use Symfony\Component\DomCrawler\Crawler;


class SentinelCommand extends ContainerAwareCommand
{
    private $octopus;

    protected function configure()
    {
        $this
            ->setName('sentinel')
            ->setDescription('Sentinel, I\'m a machine, I\'m your tool')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'the url of a website'
            )
            ->addArgument(
                'links',
                InputArgument::OPTIONAL,
                'if set, search by links'
            )
             ->addArgument(
                'images',
                InputArgument::OPTIONAL,
                'if set, search by images urls'
            )
             ->addArgument(
                'lorem',
                InputArgument::OPTIONAL,
                'if set, search by forgotten lorem ipsums'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $url = $input->getArgument('url');
        if(filter_var($url, FILTER_VALIDATE_URL) === false){
            $output->writeln($this->formatMessage('Invalid URL, try again!',0));
            exit;
        }


        if(!$this->isOnline($url)){
            $output->writeln($this->formatMessage('Target is offline or you are!',0));
            exit;
        }

      
        if ($input->getArgument('links')) $this->attackLinks($output, 1);        
        
        if ($input->getArgument('images')) $this->attackLinks($output, 2);

        if ($input->getArgument('lorem')) $this->attackText($output);

    }


    private function formatMessage($message, $type)
    {
        if($type == 0) return '<error>'.$message.'</error>'; //red back, white text
        if($type == 1) return '<info>'.$message.'</info>'; //green text
        if($type == 2) return '<comment>'.$message.'</comment>'; //yellow text
        if($type == 3) return '<question>'.$message.'</question>'; //back cyan, black text

        
    }

    private function isOnline($url)
    {
        $buzz = $this->getContainer()->get('buzz');
        $response = $buzz->get($url);
        $buzz->getClient()->setTimeout(690);
        $this->octopus = new Octopus($buzz, $url, new Crawler());
        return $this->octopus->activate();
    }

    private function attackLinks(OutputInterface $output, $type)
    {                
        $linksPage = array();
        $urlCheck = array();
        if($type == 1) $linksPage = $this->octopus->getLinksUrls();
        if($type == 2) $linksPage = $this->octopus->getImagesUrls();
        $linksFound = count($linksPage);

        $progress = new ProgressBar($output, $linksFound);

        if($linksFound == 0){
            if($type == 1) $output->writeln($this->formatMessage('No links found in '.$this->octopus->getUrl(),3));
            if($type == 2) $output->writeln($this->formatMessage('No images found '.$this->octopus->getUrl(),3));
        }


        if($linksFound > 0){

            if($type == 1) $output->writeln($this->formatMessage('Checking links in '.$this->octopus->getUrl().': ',2));
            if($type == 2) $output->writeln($this->formatMessage('Checking images in '.$this->octopus->getUrl().': ',2));
            $output->writeln('');

            $allGood = true;
            foreach ($linksPage as $link) {          
                
                $progress->advance(1);                        

                if( filter_var($link, FILTER_VALIDATE_URL) ) {
                    $status = $this->octopus->verifyAccess($link);
                    
                    if( $status >= 300 ){
                        $urlCheck[] = '['.$status.'] '.$link;                        
                        $allGood = false;
                    }            
                    
                }
            }

            if($allGood){
                $output->writeln('');
                if($type == 1) $output->writeln($this->formatMessage(' '.$linksFound.' links online!',3));
                if($type == 2) $output->writeln($this->formatMessage(' '.$linksFound.' images online!',3));
            }

            if(!$allGood){

                $output->writeln('');
                foreach ($urlCheck as $broken) {
                    $output->writeln($this->formatMessage($broken, 0));
                    $output->writeln('');
                }
                
            }
        }

        $output->writeln('');
        
    }


    private function attackText(OutputInterface $output)
    {
        $output->writeln($this->formatMessage('Checking lorem ipsumns in '.$this->octopus->getUrl().': ',2));
        $scrapTexts = $this->octopus->getMainTexts();
        $combinations = 0;
        if( is_array($scrapTexts) ){

            $progress = new ProgressBar($output, count($scrapTexts));
            $output->writeln('');

            $validatePage = new ValidatePage($this->getContainer());
                  
            foreach ($scrapTexts as $fragment) {

                $combinations += $validatePage->checkText($fragment);
                $progress->advance(1);
            }

        }

        $output->writeln('');
        $output->writeln($this->formatMessage('Lorem ipsum fragments found = '.$combinations,3));
        $output->writeln('');
        
    }
}