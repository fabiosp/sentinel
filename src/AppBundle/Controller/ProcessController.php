<?php
namespace AppBundle\Controller;


use AppBundle\Entity\WebPage;
use AppBundle\Machine\Octopus;
use AppBundle\Validation\ValidatePage;
use Symfony\Component\DomCrawler\Crawler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class ProcessController extends Controller
{
	/**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {        
        $webPage = new WebPage();
        $webPage->setUrl('');

        $form = $this->createFormBuilder($webPage)
        	->setAction($this->generateUrl('start'))
        	->setMethod("POST")
            ->add('url', UrlType::class, array(
                'label' => 'Url: ',
                'attr'  => array('style' => 'width:600px;')
                )
            )            
            ->add('save', SubmitType::class, array(
                'label' => 'Go Sentinel!',
                'attr'  => array('class' => 'action')
                )
            )            
            ->getForm();

      

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView(),
        ));
     
    }

   

    /**
     * @Route("/start", name="start")
     */
    public function beginAction(Request $request)
    {

    	$data = $request->request->get('form');
    	if($data == NULL) return $this->redirectToRoute('home');
    	
    	
    	$buzz = $this->container->get('buzz'); //https://github.com/sensiolabs/SensioBuzzBundle
        $buzz->getClient()->setTimeout(590); //isso aqui faz sentido quando o site tem mais de 200 links/imagens!
    	$octopus = new Octopus($buzz, $data['url'], new Crawler()); //este polvo depende de um crawler!

        $octopus->activate(); //nao e obrigatorio acordar a maquina, deve ser usado para verificar atualizaoes de conteudo do alvo

        
        $linksPage = $octopus->getLinksUrls();
        $linksStatus = array();
        $linksStatus = $this->dataValidationUrls($linksPage, $octopus);


        $imagesPages = $octopus->getImagesUrls();
        $imageStatus = $this->dataValidationUrls($imagesPages,$octopus);

        $totalLoremns = $this->dataValidationTexts($octopus->getMainTexts());


        $dataView = array(
            'urlUser'    => $data['url'],
            'links'      => $linksStatus,
            'images'     => $imageStatus,
            'totalLorem' => $totalLoremns,
        );
    	return $this->render('report/show.html.twig', $dataView);
        
    }


    public function dataValidationUrls($urls, $octopus)
    {
        $linksStatus = array();        
        foreach ($urls as $link) {            
                                          
            if( filter_var($link, FILTER_VALIDATE_URL) ) {
                $status = $octopus->verifyAccess($link);
                $color = 'green';
                if( $status >= 300 ) $color = 'red';

                $linksStatus[] = array(
                    'source' => '<a href="'.$link.'" target="_blank">'.$link.'</a>',
                    'status' => '[<span style="color:'.$color.'">'.$status.'</span>]',
                );
                
            }
        }
        return $linksStatus;
    }

    public function dataValidationTexts($scrapTexts)
    {
        if( !is_array($scrapTexts) ) return 0;
        $validatePage = new ValidatePage($this->container);
     
        $combinations = 0;   
        foreach ($scrapTexts as $fragment) {
            $combinations += $validatePage->checkText($fragment);
        }
        return $combinations;
        
    }
    
}