<?php
namespace AppBundle\Machine;


class Octopus
{

	private $buzz;
	private $url;
	private $crawler;
	private $contentResponse;

	/**
	 * Construtor da classe.
	 * @param $buzz buzz request service
	 * @param $url string com endereco alvo
	 * @param $crawler object DOM Crawler
	 */
	public function __construct($buzz,$url,$crawler)
	{
		$this->buzz = $buzz;
		$this->url = $url;
		$this->crawler = $crawler;		
	}

	

	public function getUrl()
	{
		return $this->url;
	}

	public function setUrl($target)
	{
		$this->url = $target;
	}


	/**
	 * Retorna codigo HTTP Response
	 * @param $crawlerHeader
	 * @return $code int
	 */
	public function getCodeRequest($crawlerHeader)
	{
		$parts = explode(" ", $crawlerHeader);	//considerando uma string de http response padrao	
		if( count($parts) <= 1) return false; //se passar uma string fora do padrao do http response retorne falso
		$code = (int)$parts[1]; //assegurando o tipo de dados		
		return $code;
	}

	/**
	 * Ativa a sentinela ou realiza a pré-configuracao da classe
	 * @return boolean retorna falso se nao e possivel alcançar o site alvo, verdadeiro do contrário
	 */ 
	public function activate()
	{		
		
		$this->contentResponse = $this->buzz->get($this->url);
		if( $this->getCodeRequest($this->contentResponse->getHeaders()[0]) != 200) return false;
				
		$this->crawler->addHtmlContent($this->contentResponse->getContent()); //melhor que crawler->add() pois podemos arrumar o 
		//charset pelo parametro opcional

		return true;						
		
	}


	/**
	 * Retorna todos os hiperlinks do site alvo
	 * @return array com as urls dos links ou array vazio se nada encontrar
	 */ 
	public function getLinksUrls()
	{
		if( !$this->contentResponse ) $this->activate();		
		$nodes = $this->crawler->filter('a');
		$links = array();
		foreach ($nodes as $domElement) {
			$links[] = $domElement->getAttribute('href');
			
		}
		return $links;
	}


	/**
	 * Retorna as urls de todas as imagens do site alvo
	 * @return array com as urls ou array vazio se nada encontrar
	 */
	public function getImagesUrls()
	{
		if( !$this->contentResponse ) $this->activate();		
		$nodes = $this->crawler->filter('img');
		$images = array();
		foreach ($nodes as $domElement) {
			$images[] = $domElement->getAttribute('src');
			
		}
		return $images;
	}


	/**
	 * Retorna todos os trechos de textos que conseguir encontrar no site alvo
	 * @param $tagName nao obrigatório, e possivel informar a tag HTML onde propcurar por textos, tag p é usada como padrão
	 * @return array com os textos encontrados, array vazio se nada encontrar
	 */ 
	public function getMainTexts($tagName = null)
	{
		if( !$this->contentResponse ) $this->activate();
		if($tagName == null) $tagName = 'p';

		$nodes = $this->crawler->filter($tagName);
		$texts = array();
		foreach ($nodes as $domElement) {
			$texts[] = $domElement->nodeValue;
			
		}
		return $texts;
	}


	/**
	 * Utilizar esas função para verificar se uma URL é acessível.
	 * @return int código HTTP status
	 */
	public function verifyAccess($url)
	{
		$response = $this->buzz->get($url);	
		return $this->getCodeRequest( $response->getHeaders()[0] );
	}

	
}