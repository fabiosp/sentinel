<?php
namespace AppBundle\Validation;

use AppBundle\Validation\Validation;

class ValidatePage implements Validation
{

	private $container;
	private $invalidFragments;

	/**
	 * Classe compatível somente com Symfony
	 * @param $serviceContainer object container de serviços do Symfony
	 */
	public function __construct($serviceContainer)
	{
		 $this->container = $serviceContainer;
		 $this->importLoremData();
	}

	public function setInvalidFragments($fragments)
	{
		$this->invalidFragments = $fragments;
	}

	public function getInvalidFragments()
	{
		return $this->invalidFragments;
	}

	/**
	 * Contabiliza quantas palavras em latim possui o texto.
	 * @param $textValue string 
	 * @return int total de combinações, retorna zero se nao for possivel contabilizar
	 */
	public function checkText($textValue)
	{
				
		if ( empty($this->invalidFragments) ) return false;
		$json = json_decode($this->invalidFragments, true);

		$textValueWords = $this->cleanText($textValue);
		$combinations = 0; //total de combinacoes com lorem ipsum

		foreach ($json as $phrase) {

			$words = array();
			$words = explode(" ", $phrase);
			
			foreach ($textValueWords as $originalWord) {
				
				foreach ($words as $latimWord) {
					
					$check = strcasecmp ( $originalWord , $latimWord ); //combinacao segura, case insensitive
					if($check == 0) $combinations++; //palavras sao identicas										
									
				}

			}
	
		}

		return $combinations;		
	}

	/**
	 * Retira de algum treecho de texto, termos desnecessários para verificação e falsos positivos, otimiza o processamento.
	 * @param $textValue string
	 * @return string filtrada
	 */
	private function cleanText($textValue)
	{
		$filteredText = array();
		//filtro para limpar o texto retirando preposicoes e outras palavras para evitar possiveis falsos positivos
		$filter = array(
			'a','á', 'à', 'e', 'é', 'por', 'para', 'perante', 'ante', 'até', 'após', 'de', 'desde', 'em', 'entre', 'com' ,
			'contra', 'sem', 'sob', 'sobre', 'trás', 'do', 'A', 'as', 'De', 'Do', 'o', 'O', 'nas', 'nos', 'das', 'dos', 
			'Para', 'Em', 'in', 'non', 'In');

		$textValueWords = explode(" ", $textValue);
		$filteredText = array_diff($textValueWords, $filter);
		return $filteredText;
	}

	/**
	 * Realiza a importação segura de arquivo texto com expressões em Lorem ipsum.
	 */
	private function importLoremData()
	{
		$this->invalidFragments = '';
		$kernel = $this->container->get('kernel');
		try{
			$path = $kernel->locateResource('@AppBundle/Extra/latim.json');
			$this->invalidFragments = file_get_contents($path);
			
		}catch(\InvalidArgumentException $e){ //arquivo nao encontrado

			echo 'File with important rules for this application not found';

		}catch(\InvalidArgumentException $e){ //arquivo com algum simbolo no nome, nao possivel de ser reconhecido

			echo 'Check file name';
		}
	}
}