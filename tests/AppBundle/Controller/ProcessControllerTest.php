<?php
namespace Tests\AppBundle\Controller;

use AppBundle\Controller\ProcessController;
use AppBundle\Machine\Octopus;
use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Validation\ValidatePage;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProcessControllerTest extends WebTestCase
{
	private $container;


	public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    // public function testIndex()
    // {
    //     $client = static::createClient();

    //     $crawler = $client->request('GET', '/');

    //     $this->assertEquals(200, $client->getResponse()->getStatusCode());
    //     $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    // }

    public function testDataValidationUrls()
    {
    	$process = new ProcessController();
    	$buzz = $this->container->get('buzz');
    	$octopus = new Octopus($buzz, 'http://google.com', new Crawler());    	
    	$this->assertInstanceOf('\AppBundle\Machine\Octopus', $octopus, 'Object Octopus is not a sentinel!');

    	$data = $process->dataValidationUrls(array(), $octopus);
    	
    	$this->assertInternalType('array', $data, 'Expected an array() of urls');

    }

    public function testDataValidationTexts()
    {
    	$process = new ProcessController();
    	$validatePage = new ValidatePage($this->container);
    	$this->assertInstanceOf('\AppBundle\Validation\Validation', $validatePage, 'Object is not Validation type');

    }
}
