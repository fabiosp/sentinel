<?php

namespace Tests\AppBundle\Machine;

use AppBundle\Machine\Octopus;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OctopusTest extends WebTestCase
{

	private $buzz;
	private $octopus;

	public function setUp()
	{
		self::bootKernel();

        $container = self::$kernel->getContainer();
		$this->buzz = $container->get('buzz');
		$this->octopus = new Octopus($this->buzz, 'http://google.com', new Crawler());		
	}


	public function testActivate()
	{
		$check = $this->octopus->activate();
		$this->assertTrue($check, 'Verify connection point or url to send octopus');

	}


	public function testGetCodeRequest()
	{
		
		$code = $this->octopus->getCodeRequest('HTTP/1.1 200');
		$this->assertInternalType('int', $code, 'Expected an int http code status');
	}


	public function testGetLinksUrls()
	{
		$data = $this->octopus->getLinksUrls();
		$this->assertInternalType('array', $data, 'Expected an array() of urls');
	}

	public function testGetImagesUrls()
	{
		$data = $this->octopus->getImagesUrls();
		$this->assertInternalType('array', $data, 'Expected an array() of images');
	}

	public function testGetMainTexts()
	{
		$data = $this->octopus->getMainTexts();
		$this->assertInternalType('array', $data, 'Expected an array() of texts');
	}

	public function testVerifyAccess()
	{
		$status = $this->octopus->verifyAccess('http://google.com');
		$this->assertInternalType('int', $status, 'status code is not a int');
	}

	
}