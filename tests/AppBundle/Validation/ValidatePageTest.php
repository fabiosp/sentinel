<?php
namespace Tests\AppBundle\Validation;

use AppBundle\Validation\ValidatePage;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ValidatePageTest extends WebTestCase
{
	private $container;
	private $validatePage;

	public function setUp()
	{
		self::bootKernel();

        $this->container = self::$kernel->getContainer();
        $this->validatePage = new ValidatePage($this->container);
	}

	public function testImportLoremData()
	{		
		$contentFile = $this->validatePage->getInvalidFragments();
		$total = strlen($contentFile);
		$this->assertNotEquals(0, $total, 'Latim or fragments file is empty!');
	}

	public function testCheckText()
	{

		$total = $this->validatePage->checkText('lorem ipsum');
		$this->assertNotEquals(0, $total, 'Lorem ipsum not detected');
			
	}
	
}